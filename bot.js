var discord = require('discord.io');
var redis = require('redis');
var moment = require('moment');
var logger = require('winston');
var auth = require('./auth.json');
var messages = require('./messages.json')

//configure logger
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';

//configure redis
var redisClient = redis.createClient();
redisClient.on('connect', function() {
    logger.info('Redis connected')
})

redisClient.on('error', function(error) {
    logger.info('Redis error detected \n' + error)
})

//configure bot
var bot = new discord.Client({
    token: auth.token,
    autorun: true
});

bot.on('ready', function(evt) {
    logger.info('Bot connected');
    logger.info('Logged in as: ' + bot.username);
    logger.info(bot.username +' - ('+ bot.id + ')');
})

bot.on('message', function(user, userID, channelID, message, evt) {
    if (message.substring(0,1) == '!') {
        var args = message.substring(1).split(' ');
        var cmd = args[0];
        switch(cmd){
            //!help
            case 'help':
                bot.sendMessage({
                    to: channelID,
                    message: messages.help
                })
            break;
            //!save 
            //Validates date, then saves key and value
            case 'save':
                if (!moment(args[2], 'MM/DD/YYYY', true).isValid()){
                    bot.sendMessage({
                        to: channelID,
                        message: args[2] + ' is not a valid date'
                    });
                }
                else{
                    logger.info('saving to redis: \'' + args[1] + '\': \'' + args[2] + '\'');
                    redisClient.set(args[1], moment(args[2], "MM/DD/YYYY", true).format("YYYYMMDD"));
                    bot.sendMessage({
                        to: channelID,
                        message: 'Countdown ' + args[1] + ' saved!'
                    });
                }
            break;
            //!get
            //retrieves and prints value for given key
            case 'get':
                redisClient.get(args[1], function(error, result){
                    if (error) {
                        logger.info('Error retreiving key ' + args[1]);
                    }
                    else if (result === null) {
                        bot.sendMessage({
                            to: channelID,
                            message: 'Sorry, that\'s not a counter that I\'m aware of.'
                        })
                    }
                    else {
                        var difference = moment(result, "YYYYMMDD", true).fromNow();
                        bot.sendMessage({
                            to: channelID,
                            message: args[1] + ' is ' + difference + ' from now!'
                        })
                    }
                })
            break;
            //!delete
            //deletes given key.
            case 'delete':
                redisClient.del(args[1])
                logger.info('deleting ' + args[1] +' from redis')
                bot.sendMessage({
                    to:channelID,
                    message: 'Deleting counter ' + args[1] 
                })
            break;
        };
    }
});